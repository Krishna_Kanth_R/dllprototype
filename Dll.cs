﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace LogRowDll
{
    public class Dll
    {
        public List<Dictionary<string, object>> ListOfRecords { get; set; }
        public List<string> ListOfFieldNames { get; set; }
        public string CsvFilePath { get; set; }
        public string LoggingFolderPath { get; set; }
        // constructor takes folder path and file path
        // creates the folder and file if doesn't exist
        public Dll(string loggingFolderPath, string csvFilePath)
        {
            LoggingFolderPath = loggingFolderPath;
            CsvFilePath = csvFilePath;
            ListOfFieldNames = new List<string>();
            // checks if directory exists
            if (!Directory.Exists(LoggingFolderPath))
            {
                Directory.CreateDirectory(LoggingFolderPath);
                // checks if file exists
                if (!File.Exists(CsvFilePath))
                {
                    //creates new file
                    using (StreamWriter writer = new StreamWriter(CsvFilePath)) ;
                }
                else
                {
                    GetFieldNamesOfExistingFile();
                }
            }
            else
            {
                // checks if file exists
                if (!File.Exists(CsvFilePath))
                {
                    using (StreamWriter writer = new StreamWriter(CsvFilePath)) ;
                    //File.Create(CsvFilePath);
                }
                else
                {
                    GetFieldNamesOfExistingFile();
                }
            }
            ListOfRecords = new List<Dictionary<string, object>>();
        }
        public void GetFieldNamesOfExistingFile()
        {
            using (var reader = new StreamReader(CsvFilePath))
            {
                reader.ReadLine();
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    csv.Read();
                    csv.ReadHeader();
                    ListOfFieldNames = csv.HeaderRecord.ToList();
                }
            }
        }
        public void AddRecord(string record)
        {
            string json = record.Replace('\'', '\"');
            var jsonObj = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
            ListOfRecords.Add(jsonObj);
            if (ListOfRecords.Count == 1000)
            {
                LogRecords();
            }
        }
        public void LogRecords()
        {
            // checks file existence
            if (File.Exists(CsvFilePath))
            {
                // checks  file is empty ? add field Name and field type : add record alone
                if (new FileInfo(CsvFilePath).Length == 0)
                {
                    // add field Name and Field type to the csv file
                    AddFieldNameAndFieldType();
                }
                // log records to csv
                LogToCSV();
                ListOfRecords.Clear();
            }
        }
        public void AddFieldNameAndFieldType()
        {
            Dictionary<string, object> firstRecord = (Dictionary<string, object>)ListOfRecords[0];
            #region Getting field names and field types from the first record of the list of records
            List<string> listOfFieldTypes = new List<string>();

            // to get list of field names and types for the column headers from the first record
            foreach (KeyValuePair<string, object> fieldType in firstRecord)
            {
                var fieldNameAndValue = JsonConvert.DeserializeObject<Dictionary<string, object>>(fieldType.Value.ToString());
                foreach (KeyValuePair<string, object> fieldName in (Dictionary<string, object>)fieldNameAndValue)
                {
                    // fieldName.Key is fieldName
                    // fieldType.Key is fieldType
                    ListOfFieldNames.Add(fieldName.Key);
                    listOfFieldTypes.Add(fieldType.Key);
                }
            }
            #endregion
            #region Logging field Names and field Types in the empty csv file as first and second row
            using (var writer = new StreamWriter(CsvFilePath, true))
            using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                // writes field Names at first row in csv file
                csv.WriteField(listOfFieldTypes);
                csv.NextRecord();
                // writes field Types at second row in csv file
                csv.WriteField(ListOfFieldNames);
                csv.NextRecord();
            }
            #endregion
        }
        public void LogToCSV()
        {
            foreach (Dictionary<string, object> record in ListOfRecords)
            {
                #region Adding New Columns for the existing csv file if the record has new field
                // conatins field Names and field type of the new field
                Dictionary<string, string> newFieldDict = new Dictionary<string, string>();

                foreach (KeyValuePair<string, object> fieldType in record)
                {
                    var fieldNameAndValue = JsonConvert.DeserializeObject<Dictionary<string, object>>(fieldType.Value.ToString());
                    foreach (KeyValuePair<string, object> fieldName in (Dictionary<string, object>)fieldNameAndValue)
                    {
                        if (!ListOfFieldNames.Contains(fieldName.Key))
                        {
                            // fieldName.Key is fieldName
                            // fieldType.Key is fieldType
                            ListOfFieldNames.Add(fieldName.Key);
                            newFieldDict.Add(fieldName.Key, fieldType.Key);
                        }
                    }
                }
                // checks if the input record has new fields, if presents, will create new columns for new fields
                if (newFieldDict.Count != 0)
                {
                    //Read all lines from the existing file
                    List<string> lines = File.ReadAllLines(CsvFilePath).ToList();
                    //add new column to the header row and Field Type in the second row
                    foreach (string key in newFieldDict.Keys)
                    {
                        lines[0] += $",{newFieldDict[key]}";
                        lines[1] += $",{key}";
                    }
                    int index = 2;
                    //add new column value for each row.
                    lines.Skip(2).ToList().ForEach(line =>
                    {
                        lines[index] += String.Concat(Enumerable.Repeat(",", newFieldDict.Count));
                        index++;
                    });

                    //write the new content to the file
                    File.WriteAllLines(CsvFilePath, lines);
                }
                #endregion
                #region Adding field values at the last row of the csv file
                // contains filed name and field value as key  value pair
                Dictionary<string, object> fieldNameAndValueDict = new Dictionary<string, object>();
                // traverse through input dictionary to get the field name and field values
                foreach (KeyValuePair<string, object> fieldType in record)
                {
                    var fieldNameAndValue = JsonConvert.DeserializeObject<Dictionary<string, object>>(fieldType.Value.ToString());
                    foreach (KeyValuePair<string, object> fieldName in (Dictionary<string, object>)fieldNameAndValue)
                    {
                        // fieldName.Key is fieldName
                        // fieldName.Key is fieldValue
                        fieldNameAndValueDict.Add(fieldName.Key, fieldName.Value);
                    }
                }
                // contains field values
                List<object> listOfFieldValues = new List<object>();
                // to add field values corresponding to the Field Names
                foreach (var fieldName in ListOfFieldNames)
                {
                    if (fieldNameAndValueDict.ContainsKey(fieldName))
                    {
                        listOfFieldValues.Add(fieldNameAndValueDict[fieldName]);
                    }
                    else
                    {
                        listOfFieldValues.Add("");
                    }
                }
                // to write the record to the csv file
                using (var writer = new StreamWriter(CsvFilePath, true))
                using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    csv.WriteField(listOfFieldValues);
                    csv.NextRecord();
                }
                #endregion
            }
        }
    }
}
